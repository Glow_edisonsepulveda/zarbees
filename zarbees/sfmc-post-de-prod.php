<?php
date_default_timezone_set("America/Chicago");
$email = $firstName = $lastName = $ZipCode = $Gender = $Child1 = $Child2 = $Child3 = $Child4 = $AnswerWhoBuy = $AnswerProduct = $response = '';

if(isset($_POST['EmailAddress'])){
    $email = $_POST['EmailAddress'];
    $subscriberKeyHash = strtolower ($email);
    $subscriberKeyHash = hash('sha256', $subscriberKeyHash);
    $subscriberKeyHash = 'ZAR_' . $subscriberKeyHash;
    $subscriberKeyHash = strtoupper($subscriberKeyHash);
}
if(isset($_POST['FirstName'])){
    $firstName = $_POST['FirstName'];
}
if(isset($_POST['LastName'])){
    $lastName = $_POST['LastName'];
}
if(isset($_POST['Gender'])){
    $Gender = $_POST['Gender'];
}
if(isset($_POST['ZipCode'])){
    $ZipCode = empty($_POST['ZipCode']) ? null : $_POST['ZipCode'];
}
if(isset($_POST['Child1'])){
    $Child1 = $_POST['Child1'];
}
if(isset($_POST['Child2'])){
    $Child2 = $_POST['Child2'];
}
if(isset($_POST['Child3'])){
    $Child3 = $_POST['Child3'];
}
if(isset($_POST['Child4'])){
    $Child4 = $_POST['Child4'];
}
if(isset($_POST['AnswerWhoBuy'])){
    $AnswerWhoBuy = $_POST['AnswerWhoBuy'];
    $AnswerWhoBuy = implode(",",$AnswerWhoBuy);
}
if(isset($_POST['AnswerProduct'])){
    $AnswerProduct = $_POST['AnswerProduct'];
    $AnswerProduct = implode(",",$AnswerProduct);
}

$errorURL = 'http://ee.zarbees.local/error';
$successURL = 'http://ee.zarbees.local/sucess';

if (!empty($email)){
    
    $isEmail = is_email($email);

    //If email doesn't exists in DE
    if($isEmail->validation == 0){
        // Request Access Token SFMC
        $accessToken = access_token();
        
        //Create register and Active Journey
        $data = array(
            "ContactKey" => $subscriberKeyHash,
            "EventDefinitionKey" => "APIEvent-2518a653-be76-6d1f-b4c3-8ab4937439bf",//Change for production
            "Data" => array(
                'EmailAddress' => $email,
                'FirstName' => $firstName,
                'LastName' => $lastName,
                'Gender' => $Gender,
                'ZipCode' => $ZipCode,
                'Child1' => $Child1,
                'Child2' => $Child2,
                'Child3' => $Child3,
                'Child4' => $Child4,
                'AnswerWhoBuy' => $AnswerWhoBuy,
                'AnswerProduct' => $AnswerProduct,
                'SubscriberKey' => $subscriberKeyHash,
                'OptStatus' => 'Y',
                'Resubscribe' => 'N',
                "CreatedDate" => date("m/d/y h:i:sa"),
                "LastModifiedDate" => date("m/d/y h:i:sa")
            ),
        );
        $dataJ = json_encode($data);

        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: Bearer ' . $accessToken;

        $ch = curl_init();

        curl_setopt_array($ch, array(
            CURLOPT_URL => "https://mc9chhxcrh236jtxg07b35fsqk-4.rest.marketingcloudapis.com/interaction/v1/events", //Change for production
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>$dataJ,
            CURLOPT_HTTPHEADER => $headers,
        ));

        $responseCurl = curl_exec($ch);

        curl_close($ch);
        $validationRegister = json_decode($responseCurl,true);
        $response = (array_key_exists('eventInstanceId',$validationRegister) == true) ? 1 : 0;
        
        $response;
    }
    //Email exists in DE
    else{
        if($isEmail->optStatus == 'Y' && $isEmail->validation == 1 ){ //optStatus is active (Y)
            $response = 0;
        }
        else { // Resubscribe process
            $url = 'http://cl.s7.exct.net/DEManager.aspx';
            $data = array(
                '_clientID' => '7306746', 
                '_deExternalKey' => '759AE716-3E85-4E1E-8226-933FC9823CD9',
                '_action' => 'add/update',
                '_returnXML' => 1,
                '_errorURL'=> $errorURL,
                '_successURL'=> $successURL,
                'EmailAddress' => $email,
                'FirstName' => $firstName,
                'LastName' => $lastName,
                'Gender' => $Gender,
                'ZipCode' => $ZipCode,
                'Child1' => $Child1,
                'Child2' => $Child2,
                'Child3' => $Child3,
                'Child4' => $Child4,
                'AnswerWhoBuy' => $AnswerWhoBuy,
                'AnswerProduct' => $AnswerProduct,
                'LastModifiedDate' => date("m/d/y h:i:sa"),
                'SubscriberKey' => $subscriberKeyHash,
                'OptStatus' => 'Y',
                'Resubscribe' => 'Y'
            );

            $fields_string = http_build_query($data);

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$fields_string);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));


            // receive server response ...
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $server_output = curl_exec ($ch);

            curl_close ($ch);

            if ( strstr( $server_output, $successURL) ) {
                $response = 1; //GOOD
            } else {
                $response = 'Something went wrong, please try again.'; //FAIL
            }
        }
    }
    
    echo $response;
}


/* Validate in LP if email exist in DE */
function is_email($email){
    $url = 'https://cloud.inform.jnj.com/zarbees-email-validator?email='.$email;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER , false);
    curl_setopt($ch, CURLOPT_ENCODING, '');
    curl_setopt($ch, CURLOPT_URL,$url);
    $result=curl_exec($ch);
    curl_close($ch);

    $result = json_decode($result);

    return $result;
}

function access_token(){
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://mc9chhxcrh236jtxg07b35fsqk-4.auth.marketingcloudapis.com/v2/token",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => array(
            'grant_type' => 'client_credentials',
            'client_id' => 'm0ezsv94tsums5vctj5sg1pl',
            'client_secret' => 'Lei8HZ2D2W4sRdRgqhYAuuKj',
            'account_id' => '7306746'
        ),
    ));
    $response = curl_exec($curl);
    $result = json_decode($response,true);
    curl_close($curl);
    return $result["access_token"];
}