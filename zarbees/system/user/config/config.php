<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['index_page'] = 'index.php';
$config['save_tmpl_files'] = 'y';
// ExpressionEngine Config Items
// Find more configs and overrides at
// https://docs.expressionengine.com/latest/general/system_configuration_overrides.html

$config['app_version'] = '5.3.1';
$config['encryption_key'] = 'd4d9fb1a4c6569f5e3ff15167fbf3659fea38338';
$config['session_crypt_key'] = '7a8b68d7b01db8dc88a139ba77f1bfef800998f4';
$config['database'] = array(
	'expressionengine' => array(
		'hostname' => 'localhost',
		'database' => 'jj_zarbees',
		'username' => 'root',
		'password' => '',
		'dbprefix' => 'exp_',
		'char_set' => 'utf8mb4',
		'dbcollat' => 'utf8mb4_unicode_ci',
		'port'     => ''
	),
);
$config['share_analytics'] = 'y';

$default_global_vars = [
    'global:disabled_params' => 'disable="trackbacks|pagination|member_data"',
    'global:disabled_params_all' => 'disable="trackbacks|pagination|member_data|category_fields|categories|custom_fields"',
    'global:disabled_params_strict' => 'disable="trackbacks|pagination|member_data|category_fields|categories"',
    'global:blog_category_groups' => '2|3|5|6',
    'global:some_api_key' => '98ufahaskdfnasd312',
];

if (!isset($assign_to_config['global_vars'])) {
    $assign_to_config['global_vars'] = [];
}

$assign_to_config['global_vars'] = array_merge(
    $default_global_vars,
    $assign_to_config['global_vars']
);
// EOF