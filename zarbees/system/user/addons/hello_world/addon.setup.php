<?php
return [
  'author'         => 'Developer James',
  'author_url'     => 'https://example.com/',
  'name'           => 'Hello World',
  'description'    => 'Outputs a simple "Hello World" message to test plugins!',
  'version'        => '1.0.0',
  'namespace'      => 'DeveloperJames\HelloWorld',
  'settings_exist' => FALSE,
];