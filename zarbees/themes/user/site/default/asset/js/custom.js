$(function () {
    $('#modal_newsletter').click(function () {
        $('html').addClass('modal-open');
        $('.wrapper-pop-up').addClass('show-pop-up');
    });

    $('.alpha').on('keyup', function (event) {
        $(this).val($(this).val().replace(/[^a-zA-Z\s\-]/, ""));
    });
    $('.alpha').on('paste', function (event) {
        if (!event.originalEvent.clipboardData.getData('Text').match(/^[a-zA-Z\s\-]+$/)) {
            event.preventDefault();
        }
    });

    $('.numeric').on('keyup', function (event) {
        $(this).val($(this).val().replace(/[^0-9]/, ""));
    });
    $('.numeric').on('paste', function (event) {
        if (!event.originalEvent.clipboardData.getData('Text').match(/^[0-9]+$/)) {
            event.preventDefault();
        }
    });

    $('#newsletter-form-id .close').click(function () {
        $('html').removeClass('modal-open');
        $('.wrapper-pop-up').removeClass('show-pop-up');
        $('.thank-msg').removeClass('show');
        $("#newsletter-form-id form").trigger("reset");
    });

    $("#newsletter-form-sfmc").submit(function (e) {
        e.preventDefault();
        var AnswerProduct = [];
        $('input[name="AnswerProduct"]:checked').each(function () {
            if ($(this).is(':checked')) {
                AnswerProduct.push(this.value);
            }
        });

        var AnswerWhoBuy = [];
        $('input[name="AnswerWhoBuy"]:checked').each(function () {
            if ($(this).is(':checked')) {
                AnswerWhoBuy.push(this.value);
            }
        });

        var email = $('#newsletter-form-sfmc input[name="EmailAddress"]').val();
        var firstname = $('#newsletter-form-sfmc input[name="FirstName"]').val();
        var lastname = $('#newsletter-form-sfmc input[name="LastName"]').val();
        var Gender = $('#newsletter-form-sfmc select[name="Gender"]').val();
        var ZipCode = $('#newsletter-form-sfmc input[name="ZipCode"]').val();
        var Child1 = $('#newsletter-form-sfmc select[name="Child1"]').val();
        var Child2 = $('#newsletter-form-sfmc select[name="Child2"]').val();
        var Child3 = $('#newsletter-form-sfmc select[name="Child3"]').val();
        var Child4 = $('#newsletter-form-sfmc select[name="Child4"]').val();
        var submitUrl = $(this).attr('action');

        var requiredFields = ['email', 'first-name', 'last-name'];

        $.each(requiredFields, function (i, l) {
            $("#newsletter-form-sfmc #newsletter-" + l + "").keyup(function () {
                $(this).removeClass('-invalid');
            });
        });

        var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
        var EmailValid = false;
        if (testEmail.test(email)) {
            $("#newsletter-form-sfmc #newsletter-email").removeClass('-invalid');
            EmailValid = true;
        } else {
            $("#newsletter-form-sfmc #newsletter-email").addClass('-invalid');
        }

        if (email == '' || firstname == '' || lastname == '' || EmailValid == false) {
            $.each(requiredFields, function (i, l) {
                if ($("#newsletter-form-sfmc #newsletter-" + l + "").val() == "") {
                    $("#newsletter-form-sfmc #newsletter-" + l + "").addClass('-invalid');
                }
            });
        } else {
            $.ajax({
                type: "post",
                url: submitUrl,
                data: {
                    EmailAddress: email,
                    FirstName: firstname,
                    LastName: lastname,
                    Gender: Gender,
                    ZipCode: ZipCode,
                    Child1: Child1,
                    Child2: Child2,
                    Child3: Child3,
                    Child4: Child4,
                    AnswerWhoBuy: AnswerWhoBuy,
                    AnswerProduct: AnswerProduct
                },
                success: function (responseData, textStatus, jqXHR) {
                    if (responseData == 1) {
                        $('.thank-msg').addClass('show');
                    } else if (responseData == 0) {
                        alert('Your email is already registered.');
                    }
                    else {
                        alert(responseData);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            })
        }
    })
});