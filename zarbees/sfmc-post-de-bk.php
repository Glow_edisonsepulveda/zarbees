<?php 
    
    $email = $firstName = $lastName = $Gender = $ZipCode = $Child1 = $Child2 = $Child3 = $Child4 = $AnswerWhoBuy = $AnswerProduct = $response = '';
    
    if(isset($_POST['EmailAddress'])){
        $email = $_POST['EmailAddress'];
        $subscriberKeyHash = strtolower ($email);
        $subscriberKeyHash = hash('sha256', $subscriberKeyHash);
        $subscriberKeyHash = 'UAT_ZAR_' . $subscriberKeyHash;
        $subscriberKeyHash = strtoupper($subscriberKeyHash);
    }
    if(isset($_POST['FirstName'])){
        $firstName = $_POST['FirstName'];
    }
    if(isset($_POST['LastName'])){
        $lastName = $_POST['LastName'];
    }
    if(isset($_POST['Gender'])){
        $Gender = $_POST['Gender'];
    }
    if(isset($_POST['ZipCode'])){
        $ZipCode = $_POST['ZipCode'];
    }
    if(isset($_POST['Child1'])){
        $Child1 = $_POST['Child1'];
    }
    if(isset($_POST['Child2'])){
        $Child2 = $_POST['Child2'];
    }
    if(isset($_POST['Child3'])){
        $Child3 = $_POST['Child3'];
    }
    if(isset($_POST['Child4'])){
        $Child4 = $_POST['Child4'];
    }
    if(isset($_POST['AnswerWhoBuy'])){
        $AnswerWhoBuy = $_POST['AnswerWhoBuy'];
        $AnswerWhoBuy = implode(",",$AnswerWhoBuy);
    }
    if(isset($_POST['AnswerProduct'])){
        $AnswerProduct = $_POST['AnswerProduct'];
        $AnswerProduct = implode(",",$AnswerProduct);
    }

    $errorURL = 'http://ee.zarbees.local/error';
    $successURL = 'http://ee.zarbees.local/sucess';

    if( !empty($email)){
        /* $url = 'https://cloud.inform.jnj.com/zarbees-email-validator?email='.$email; */
        $url = 'https://pub.s7.exacttarget.com/clnesq4jeum?email='.$email;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER , false);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_URL,$url);
        $result=curl_exec($ch);
        curl_close($ch);

        $result = json_decode($result);

        if($result->validation == "0" ){ //email not exist
            if($result->optStatus == "Y"){
                $response = 1;
            }
            else{
                $url = 'http://cl.s7.exct.net/DEManager.aspx';
                $data = array(
                    /* '_clientID' => '7306746', 
                    '_deExternalKey' => '759AE716-3E85-4E1E-8226-933FC9823CD9', */
                    '_clientID' => '7327101', 
                    '_deExternalKey' => '3F08AD45-F3A1-4308-AF59-DDD6E546AC0C',
                    '_action' => 'add',
                    '_returnXML' => 1,
                    '_errorURL'=> $errorURL,
                    '_successURL'=> $successURL,
                    'EmailAddress' => $email,
                    'FirstName' => $firstName,
                    'LastName' => $lastName,
                    'Gender' => $Gender,
                    'ZipCode' => $ZipCode,
                    'Child1' => $Child1,
                    'Child2' => $Child2,
                    'Child3' => $Child3,
                    'Child4' => $Child4,
                    'AnswerWhoBuy' => $AnswerWhoBuy,
                    'AnswerProduct' => $AnswerProduct,
                    'SubscriberKey' => $subscriberKeyHash,
                    'OptStatus' => 'Y',
                    'Resubscribe' => 'N'
                );

                $fields_string = http_build_query($data);

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS,$fields_string);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));


                // receive server response ...
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $server_output = curl_exec ($ch);

                curl_close ($ch);

                if ( strstr( $server_output, $successURL) ) {
                    $response = 1; //GOOD
                } else {
                    $response = 'Something went wrong, please try again.'; //FAIL
                }
            }
        }
        else{
            $response = 0; // Email exist
        }

        echo $response;
    }
?>